package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", handler)

	http.ListenAndServe(":"+os.Getenv("PORT"), nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
	hn, _ := os.Hostname()
	tpl := fmt.Sprintf("Shipped and delivered by shippable \nHostname: %s", hn)

	w.Write([]byte(tpl))
}
