package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func MainTest(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)

	if err != nil {
		t.Error(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(handler)

	handler.ServeHTTP(recorder, req)

	if recorder.Code != 200 {
		t.Errorf("Expected response code %d; got: %d", 200, recorder.Code)
	}

	expectedBody := "Shipped and delivered by shippable"
	body := recorder.Body.String()

	if !strings.Contains(body, expectedBody) {
		t.Errorf("Expected body %s; got: %s", expectedBody, body)
	}
}
